function loss= ...
    comp_Loss_Tranche( ...
    nb_cds_def, ...
    attach_do, ...
    attach_up, ...
    nb_cds_init, ...
    nominal_cds, ...
    recovery ...
    )
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
L=(1-recovery)*nominal_cds;
xd=nb_cds_init*attach_do;
xu=nb_cds_init*attach_up;
nd=comp_integer(xd);
nu=comp_integer_mod(xu);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loss1=0;
loss2=L*(nb_cds_def-xd);
loss3=L*(xu-xd);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (0<=nb_cds_def && nb_cds_def<=nd)
    loss=loss1;
elseif  (1+nd<=nb_cds_def && nb_cds_def<=nu)
    loss=loss2;
elseif (1+nu<=nb_cds_def && nb_cds_def<=nb_cds_init)
    loss=loss3;
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
end