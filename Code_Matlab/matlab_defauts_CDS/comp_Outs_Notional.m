function notional= ...
    comp_Outs_Notional( ...
    nb_cds_def, ...
    attach_do, ...
    attach_up, ...
    nb_cds_init, ...
    nominal_cds, ...
    recovery ...
    )
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
L=(1-recovery)*nominal_cds;
xd=nb_cds_init*attach_do;
xu=nb_cds_init*attach_up;
nd=comp_integer(xd);
nu=comp_integer_mod(xu);
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
notional1=L*(xu-xd);
notional2=L*(xu-nb_cds_def);
notional3=0;
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (0<=nb_cds_def && nb_cds_def<=nd)
    notional=notional1;
elseif  (1+nd<=nb_cds_def && nb_cds_def<=nu)
    notional=notional2;
elseif (1+nu<=nb_cds_def && nb_cds_def<=nb_cds_init)
    notional=notional3;
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
end