function n=comp_integer_mod(x)
%% %%%%%%%%%%%%%%%%%%%%%%%%%% %%
z=floor(x)-x;
if z== 0
    n=x-1;
else
    n=floor(x);
end
%% %%%%%%%%%%%%%%%%%%%%%%%%%% %%
end