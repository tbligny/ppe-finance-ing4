function y=f(PTBar,RoT,z)
y=normcdf( (norminv(PTBar) - (sqrt(RoT))*z)/sqrt(1-RoT) );
end