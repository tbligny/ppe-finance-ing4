tic;
clear all; close all;
%if matlabpool('size') == 0 % check if matlabpool is running
%    matlabpool OPEN        % open it if that's the case
%end

nb_scenarios = 3;
%% PARTIE CLEMENT & ANTOINE : STRATEGIES
compteur=0;
for i = 1: 4095
    a(i)  = i;
    if ( (bitand(a(i),2048) & bitand(a(i),32)) | (bitand(a(i),1024) & bitand(a(i),16)) | (bitand(a(i),512) & bitand(a(i),8)) | (bitand(a(i),256) & bitand(a(i),4)) | (bitand(a(i),128) & bitand(a(i),2)) | (bitand(a(i),64) & bitand(a(i),1)))
        a(i) =0;
    end
    if (a(i) ~= 0) compteur = compteur+1;
        b(compteur) = a(i);
        c=dec2bin(b,12);
    end
end
%% c is the vector of strategies
achats=c(:,1:6);
ventes=c(:,7:12);
%numero_strategie = 708 % de 1 a 728

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
esperance_profit_loss = zeros(size(achats,1), nb_scenarios) % 728 lignes, nb colonnes = nb scnenarios = q
N = 125;  % number of CDSs
% correlation coefficient (un scenario par colonne)
    RoT = [
        0.01 0.015  0.3
        ];
% proba non conditionnelle (un scenario par colonne, temps pour les lignes)
    PTBar = [
        0.01    0.01    0.3
        0.015   0.03    0.4
        0.02    0.04    0.5
        0.025   0.05    0.5
        0.03    0.06    0.5
        0.035   0.07    0.6
        ];
    nbSim = 10;               % number of scenarios
    T=size(PTBar,1);    
    matDefauts = zeros(nbSim,T);
    vect = zeros(size(PTBar,1),N+1);
    q = zeros(size(PTBar,1),N);
for w=1:nb_scenarios % nombre de scenarios
    scenario = w; %numero du scenario
    for l=1:size(PTBar,1)
        for j=0:N           % put parfor here fo parallel for loop (multiprocessing)
            % perform the integral calculation * binomial coefficient
            vect(l,j+1)=probaNt(j,N,PTBar(l,scenario),RoT(1,scenario));
        end
        q(l,1)=vect(l,1);
        for i=2:N+1
            q(l,i) = q(l,i-1) + vect(l,i);
        end
    end
    
    % first column <=> time t_1
    for i=1:nbSim
        randomNumber = rand(1);
        h=1;
        while q(1,h) < randomNumber
            h = h+1;
        end
        matDefauts(i,1) = h-1;
    end
    % other columns
    for k=2:T
        for i=1:nbSim
            while(matDefauts(i,k) < matDefauts(i,k-1))
                randomNumber = rand(1);
                h=1;
                while q(k,h) < randomNumber
                    h = h+1;
                end
                matDefauts(i,k) = h-1;
            end
        end
    end
    
    % matDefauts % matrix of defaults at each time
    %defauts = linspace(0,N,N+1);
    %
    % temps = linspace(1,size(PTBar,1),size(PTBar,1));
    %
    % hold on
    %
    %  subplot(3,1,1)
    % plot(defauts,vect,'b*-');
    % xlabel('number of defaults')
    % ylabel('associated probability (pdf)')
    %  subplot(3,1,2)
    %   plot(defauts,q,'r-');
    % % %plot(defauts,q(1,:),'ro-');
    %  xlabel('number of defaults')
    %  ylabel('cumulated probability (cdf)')
    %  subplot(3,1,3)
    %  plot(temps,matDefauts,'m+--');
    %  xlabel('time')
    %  ylabel('number of defaults')
    %
    % hold off
    
    
    %% script_Exo1_Strategy_CDO %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
    
    nb_cds_init=N; %125
    recovery=40/100;
    nominal_cds= 10;
    delta=0.5;
    vec_t=delta*(1:1:6)';
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    attach_do_Tr1=0/100;
    attach_up_Tr1=3/100;
    
    attach_do_Tr2=3/100;
    attach_up_Tr2=6/100;
    
    attach_do_Tr3=6/100;
    attach_up_Tr3=9/100;
    
    attach_do_Tr4=9/100;
    attach_up_Tr4=12/100;
    
    attach_do_Tr5=12/100;
    attach_up_Tr5=20/100;
    
    attach_do_Tr6=20/100;
    attach_up_Tr6=100/100;
    
    basis=10000;
    %prime_cdo_Tr1=250/basis;
    %prime_cdo_Tr2=175/basis;
    prime_cdo_Tr1=500/basis;
    prime_cdo_Tr2=250/basis;
    prime_cdo_Tr3=175/basis;
    prime_cdo_Tr4=150/basis;
    prime_cdo_Tr5=100/basis;
    prime_cdo_Tr6=25/basis;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_param_cdo=[...
        nb_cds_init, ...
        nominal_cds, ...
        recovery, ...
        delta ....
        ];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_param_Tr1=[ ...
        attach_do_Tr1,...
        attach_up_Tr1, ...
        prime_cdo_Tr1 ...
        ];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_param_Tr2=[ ...
        attach_do_Tr2,...
        attach_up_Tr2, ...
        prime_cdo_Tr2 ...
        ];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_param_Tr3=[ ...
        attach_do_Tr3,...
        attach_up_Tr3, ...
        prime_cdo_Tr3 ...
        ];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_param_Tr4=[ ...
        attach_do_Tr4,...
        attach_up_Tr4, ...
        prime_cdo_Tr4 ...
        ];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_param_Tr5=[ ...
        attach_do_Tr5,...
        attach_up_Tr5, ...
        prime_cdo_Tr5 ...
        ];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_param_Tr6=[ ...
        attach_do_Tr6,...
        attach_up_Tr6, ...
        prime_cdo_Tr6 ...
        ];
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
    n0=0;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    vec_result=zeros(size(PTBar,1),2,nbSim);
    size_vec_nb_cds_def = size(PTBar,1);
    
    for m=1:size(achats,1) % de 1 a 728
        %for m=1:10
        for j=1:nbSim
            
            vec_nb_cds_def=transpose(matDefauts(j,:)); % une ligne de defaut pour une sim
            
            
            n=zeros(1,size_vec_nb_cds_def);
            for i=2:1:size_vec_nb_cds_def+1
                n(i)=vec_nb_cds_def(i-1);
            end
            
            %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
            %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
            
            
            
            accr_CF_strategy_time=zeros(1,size_vec_nb_cds_def);
            for i=1:1:size_vec_nb_cds_def
                accr_CF_strategy_time(i)=...
                    comp_accr_CF_strategy( ...
                    n(i), ...
                    n(i+1), ...
                    vec_param_Tr1, ...
                    vec_param_Tr2, ...
                    vec_param_Tr3, ...
                    vec_param_Tr4, ...
                    vec_param_Tr5, ...
                    vec_param_Tr6, ...
                    vec_param_cdo, ...
                    achats, ... %%%
                    ventes, ...  %%%
                    m ... %%% m = numero de la strategie : ligne
                    );
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %cum_CF_strategy_time0=0;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            cum_CF_strategy_time=zeros(1,size_vec_nb_cds_def+1);
            for i=1:1:size_vec_nb_cds_def
                cum_CF_strategy_time(i+1)= ...
                    cum_CF_strategy_time(i)+accr_CF_strategy_time(i);
            end
            
            
            
            %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
            %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
            % answ_Q1=cum_CF_strategy_time1
            % answ_Q2=cum_CF_strategy_time2
            % answ_Q3=cum_CF_strategy_time3
            %
            % answ_Q4=cum_CF_strategy_time4
            % answ_Q5=cum_CF_strategy_time5
            % answ_Q6=cum_CF_strategy_time6
            
            
            %vec_result=zeros(size_vec_nb_cds_def,2);
            %for i=1:1:size_vec_nb_cds_def
            %vec_result(i,1) = accr_CF_strategy_time(i);
            %vec_result(i,2) = cum_CF_strategy_time(i+1);
            %end
            
            for i=1:1:size_vec_nb_cds_def
                vec_result(i,1,j) = accr_CF_strategy_time(i);
                vec_result(i,2,j) = cum_CF_strategy_time(i+1);
            end
            
        end %FINAL
        
        %vec_result
        for i=1:nbSim
            vec_plot(i)=vec_result(size_vec_nb_cds_def,2,i);
        end
        
        esperance_profit_loss(m,w)=mean(vec_plot); % q = numero de scenario
    end % 728 strategies
end
esperance_profit_loss
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hold on
% subplot(2,1,1)
% plot(vec_plot,'r*');
% subplot(2,1,2)
% plot(matDefauts,'x');
% hold off
% matDefauts

%% ACP
[pc,score,latent,tsquare] = pca(esperance_profit_loss);
dlmwrite('pc.txt',pc);
dlmwrite('score.txt',score);

%% Load from files
pc = importdata('pc.txt');
score = importdata('score.txt');
%pc,score,latent,tsquare
%cumsum(latent)./sum(latent);

        % score2 = zeros();
        % k=1;
        % for j=1:size(score,2) % 3
        %     for i=1:size(score,1) % 728
        %         if score(i,j) > 0
        %             score2(k,j) = score(i,j);
        %             k=k+1;
        %         end
        %     end
        % end
        % score2
        
biplot(pc(:,1:3),'Scores',score(:,1:3),'VarLabels',...
    {'Scenario 1' 'Scenario 2' 'Scenario 3'}) % biplot --> ACP
%pc(:,1:3)
%score(100,1:3)
%%

%% Score scaled
scoreScaled = zeros(728,3);
for j=1:size(score,2) % 3
    for i=1:size(score,1) % 728
        scoreScaled(i,j) = (score(i,j)/max(abs(score(:,j)))) * max( abs( pc(:,j) ) );
    end
end
%scoreScaled

distances = zeros(728,3);
for j=1:3
for i=1:728
   X = [pc(j,:);scoreScaled(i,:)];  % x1,y1,z1;x2,y2,z2
   distances(i,j) =  pdist(X,'euclidean'); % colonne 1 : distance euc entre strat1:728 et scenario1, etc
end
end
%scoreScaled(1:728,:)
%pc(1,:)
%distances % distances euclidiennes par rapport ? chaque scenario
%d = pdist(X,'euclidean')


strategieEtDistances = [bin2dec(c) distances]
strategieEtDistancesTriees = sortrows(strategieEtDistances,4) % trier par ordre croissant une colonne (4e) en synchronisant les autres colonnes

dec2bin(strategieEtDistancesTriees(2,1),12)
%%

%%
toc

%mean(vec_plot)
%median(vec_plot)