function y=probaNt(j,N,PTBar,RoT)
a = @(z,PTBar,RoT,j,N) ((f(PTBar,RoT,z)).^j).*((1-f(PTBar,RoT,z)).^(N-j)).*phi(z);
%binom=double(nchoosek(vpi(N),j));
%integrale=quadgk(@(z)a(z,PTBar,RoT,j,N),-Inf,Inf);
%y=binom*integrale;
y=nchoosek(N,j)*quadgk(@(z)a(z,PTBar,RoT,j,N),-Inf,Inf);
end
