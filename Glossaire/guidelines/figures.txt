
------------------------- 1 figure------------------------------

\begin{figure}[H]
	\centering
		\includegraphics[width=0.5\textwidth]
%include one of the following linies, depending on which part the figure belongs to
%{analysis/pictures/"picture"}
%{conclusion/pictures/"picture"}
%{appendix/pictures/"picture"}
%{requirements/pictures/"picture"}
%{design/pictures/"pciture"}
\caption{"desciption"}



%\label{fig:"LABEL"}%

\end{figure} 

----------------------------- 2 figurer ------------------------
\begin{figure}[H]
	\centering
	\subfigure [Desciption to  Figure-1]
	{
	\includegraphics[width=0.36\textwidth]
	%include one of the following linies, depending on which part the figure belongs to	
	%{analysis/pictures/"picture"}
	%{Conclusion/pictures/"picture"}
	%{appendix/pictures/"picture"}
	%{requirements/pictures/"picture"}
	%{design/pictures/"pciture"}
	\caption{"desciption"}
	%\label{fig:"LABEL"}%
	}
	\subfigure [Desciption to Figure-2]
	{
	\includegraphics[width=0.36\textwidth]
	%include one of the following linies, depending on which part the figure belongs to	
	%{analysis/pictures/"picture"}
	%{Conclusion/pictures/"picture"}
	%{appendix/pictures/"picture"}
	%{requirements/pictures/"picture"}
	%{design/pictures/"pciture"}
	\caption{"desciption"}
	%\label{fig:"LABEL"}%
	}
	\caption{Desciption}
\end{figure} 



------------------------ Matlab FIX ----------------------------
-----

1. Save as .fig 
2. Save as .eps
3. use epstopdf "fil.eps"
4. Include the pdf in Latex


epstopdf:
-------Windows:

k�r http://www.imagemagick.org/download/binaries/ImageMagick-6.7.2-9-Q16-windows-dll.exe
og bare NEXT NEXT...... 

Use the CMD. Find the directory whre the file is saved and type:

epstopdf "fil.eps"


-------Linux: 


sudo apt-get install imagemagick


Use the Terminal. Find the directory whre the file is saved and type:
	

epstopdf "fil.eps"

