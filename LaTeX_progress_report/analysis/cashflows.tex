\chapter{Cashflows associated to CDOs}
\textit{This chapter describes the way cash flows are calculated given a certain number of inputs and outputs.}

 
\section{Mechanisms of CDOs}


Let's consider the tranche $Tr\equiv[K_{d}--K_{u}]$ of a CDO, whose underlying portfolio is supposed to be made of $N$ CDSs, each with an identical nominal value $\cal{P}$. Thus the underlying portfolio is supposed homogeneous and the hedging rate associated to each entity is $R$.


We assume that the purchase and sale operations associated to the hedging on tranche $Tr$ are made for maturity $t_{m}$ at rate $\pi$.


We denote $t_{k}$ the payment moments with $k=1,..,m$, and $\delta=t_{k}-t_{k-1}$ the time lapse between two payments. We denote $n_{k}$ the number of CDSs, which default at time $t_{k}$.


For each moment $t_{k}$, the seller and the buyer must pay respectively the amounts ${\cal{D}\cal{L}}_{k}$ et ${\cal{P}\cal{L}}_{k}$ such that:
 

\begin{eqnarray}
&\label{eq:LM}{\cal{D}\cal{L}}_{k}={\cal{L}}_{k}-{\cal{L}}_{k-1}\\
 &\label{eq:LF}{\cal{P}\cal{L}}_{k}=\frac{1}{2}\times \pi \times ({\cal{N}}_{k}+{\cal{N}}_{k-1})\times \delta
\end{eqnarray}

\begin{table}[H]
\begin{tabular}{l l l}
where
& ${\cal{L}}_{k}$ & refers to the associated loss to tranche $Tr$ at time $t_k$\\
& ${\cal{N}}_{k}$ & refers to the reference nominal for the premium payment at time $t_k$\\
\end{tabular}
\end{table} 

\begin{figure}[H] %\label{fig:structure_credit}
	\centering
		\includegraphics[width=0.85\textwidth]{analysis/images/structure_credit.pdf}
\caption{General structure of a credit derivative. ${\cal{P}\cal{L}}_{k}$ refers to the regular premium payments while ${\cal{D}\cal{L}}_{k}$ refers to the contingent payments.}
\end{figure}


The legs in \autoref{eq:LM} et \autoref{eq:LF} depend on $n_{k}$. Thus it is essential to determine bounds for $n_{k}$ with respect to those of $K_{d}$ and $K_{u}$ of $Tr$. 

We introduce two threshold values:
\begin{eqnarray}
& x_{d}=N\times K_{d} \\
& x_{u}=N\times K_{u}
\end{eqnarray}
 
Furthermore, we set:
\begin{eqnarray}
{\cal{K}}=(1-R)\times {\cal{P}}
\end{eqnarray}

With ${\cal{L}}_k$ being linked to ${\cal{N}}_k$ by the following relation:
 
\begin{eqnarray}
\label{eq:LN}{\cal{N}}_k={\cal{K}}\times (x_u-x_d)-{\cal{L}}_k
\end{eqnarray}

Besides, we note that $k\in[1,m]$, $n_{k}\in\mathbb{N}$, which is not necessarily the case of $x_d$ and $x_u$. Therefore we build two integers $n_d$ and $n_u$ such that:  

\begin{eqnarray}
n_{d} &=&\lfloor x_d\rfloor \qquad \lfloor x_d\rfloor \text{ being the integer part of }x_d\\
n_{u} &=& 
\left\{ 
\begin{array}{l l}
  \lfloor x_u\rfloor-1 & \quad \text{if $x_u\in\mathbb{N}$}\\
  \lfloor x_u\rfloor & \quad \text{otherwise}\\ 
 \end{array} 
\right.
\end{eqnarray}

We express ${\cal{L}}_k$ with respect to the number of defaults $n_k$:

\begin{eqnarray}
  {\cal{L}}_k= 
\left\{ 
\begin{array}{l l l}
   0 & \quad \text{if $0 \le n_k \le n_d$}\\
  {\cal{K}}\times (n_k-x_d) & \quad \text{if $1+n_d \le n_k \le n_u$}\\
  {\cal{K}}\times (x_u-x_d) & \quad \text{otherwise}\\ 
\end{array} 
\right.\\
\end{eqnarray}
Finally, thanks to \autoref{eq:LN} we deduce the following expression of ${\cal{N}}_k$ with respect to $n_{k}$:
\begin{eqnarray}
{\cal{N}}_k= 
\left\{ 
  \begin{array}{l l l}
     {\cal{K}}\times (x_u-x_d) & \quad \text{if $0 \le n_k \le n_d$}\\
    {\cal{K}}\times (x_u-n_k) & \quad \text{if $1+n_d \le n_k \le n_u$}\\
    0 & \quad \text{otherwise}\\ 
\end{array} 
\right.
\end{eqnarray}

This way we have described the cash flow increases associated to the sale and purchase of hedging at each time $t_k$ of payment with respect to the number of defaults $n_k$. 

\begin{figure}[H]
	\centering
		\includegraphics[width=\textwidth]{analysis/images/cashflow_orga.pdf}
\caption{Flowchart summing up the evaluation of cash flows for a buyer and a seller.}
\label{fig:cashflow_orga}
\end{figure}

\autoref{fig:cashflow_orga} summarizes schematically the process to evaluate cash flows. $k$ varies from $1$ to $m$, where $t_m$ is the maturity. $n_k$ is the number of defaults at $t_k$, $CF{\cal{S}}_k=CF{\cal{S}}_{k-1}+{\cal{P}\cal{L}}_{k}-{\cal{D}\cal{L}}_{k}$ is the cumulated cash flow at $t_k$ for the seller and $CF{\cal{B}}_k=CF{\cal{B}}_{k-1}+{\cal{D}\cal{L}}_{k}-{\cal{P}\cal{L}}_{k}$ the cumulated cash flow at $t_k$ for the buyer.