

--Enkelte linjer kode ----
Use this line when changing language
\lstset{language = {"LANG"}}



%\lstset{language = {[x86masm]Assembler}, caption={"CAPTION"}, label={"list_navn"}} %brug den her til Assembler-kode indtil videre



\begin{lstlisting}[caption={"caption},label={lst:"name"}]
%KODE
\end{lstlisting}

With border:
\begin{lstlisting}[caption={},label={},frame=single, rulecolor=\color{black}]
%KODE
\end{lstlisting}

Leave caption blank to have the listing be unnumbered

--Source-file-----
\lstset{language = {"LANG"}, caption={"CAPTION"}, label={"list_navn"}}
\lstinputlisting{"source_filename"."ext"}
